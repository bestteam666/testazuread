import { AuthenticationParameters, Configuration } from "msal";
import { AuthOptions, CacheOptions } from "msal/lib-commonjs/Configuration";
import { MsalAuthProvider, LoginType, IMsalAuthProviderConfig } from "react-aad-msal";


const auth: AuthOptions = {
    authority: "https://login.microsoftonline.com/2a916b80 - 031c- 46f9-8f14 - 884c576496ca",
    clientId: "1b715bba-f119-4dfa-b494-75866d52283b",
    postLogoutRedirectUri: window.location.origin,
    redirectUri: window.location.origin,
    validateAuthority: true,
    navigateToLoginRequestUrl: true,
};
const cache: CacheOptions = {
    cacheLocation: "localStorage",
    storeAuthStateInCookie: false
};

const authenticationParameters: AuthenticationParameters = {
    scopes: [
        'api://7d436785-d1b9-4557-89cb-9b937f5ef530'
    ]
}
const options: IMsalAuthProviderConfig = {
    loginType: LoginType.Redirect
}

const config: Configuration = { auth, cache };
export const authProvider: MsalAuthProvider = new MsalAuthProvider(config, authenticationParameters, options)